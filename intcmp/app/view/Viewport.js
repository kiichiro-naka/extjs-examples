Ext.define('Test.view.Viewport', {
	extend: 'Ext.container.Viewport',
	alias: 'widget.main-viewport',
	uses: [
		"Test.view.inner.Counter",
	],
	items: [
		{
			xtype: "label",
			region: "north",
			text: "Label",
		},
		{
			xtype: "container",
			region: "center",
			itemId: "counters",
		},
		{
			xtype: "button",
			region: "north",
			text: "Reset All",
			itemId: "reset",
		},
	],
	layout: "border",
	initComponent: function () {
		var me = this;
		me.callParent(arguments);
		var label = me.down("label")
		var button_reset = me.down("button#reset")
		var container_counters = me.down("container#counters")
		var counter = null;
		for (var i = 0; i < 3; i ++) {
			counter = Ext.widget({
				xtype: "counter",
				region: "north",
				itemId: "item" + i,
				style: { borderColor: "#000000", borderStyle: "solid" },
				border: 1,
			})
			counter.on("value_changed", function (counter) {
				label.setText(counter.itemId + " has been changed.");
			})
			container_counters.add(counter)
		}
		button_reset.on("click", function () {
			container_counters.items.each(function (counter) {
				counter.setValue(0)
			})
		})
		Ext.util.Observable.capture(me, function () {
			console.log(arguments);
		});
	},
});
