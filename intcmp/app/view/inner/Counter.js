Ext.define('Test.view.inner.Counter', {
	extend: 'Ext.container.Container',
	alias: 'widget.counter',
	itemId: "default_counter",
	items: [
		{
			xtype: "textfield",
			itemId: 'counter',
			text: "Counter",
			region: "center",
		}, {
			xtype: "container",
			region: "south",
			items: [
				{
					xtype: "button",
					itemId: "increment",
					text: "↑",
					region: "west",
				}, {
					xtype: "component",
					html: "<div foo=bar><input type=button class=decrement value='↓'></div>",
					region: "west",
				},
			],
		},
	],
	initComponent: function () {
		var me = this;
		me.callParent(arguments);
		var counter = 0; // カウンタ変数
		/// 以下のように、必要な Ext JS コンポーネントをクエリする。
		/// getComponent() だとコンテナ直下しか取れないので不可
		var textfield_counter = me.down("textfield#counter")
		var button_increment = me.down("button#increment")
		/// カウンタをセットする、public なインスタンスメソッド
		me.setValue = function (value) {
			counter = value;
			textfield_counter.setValue(counter)
		}
		/// 値が変えられたらイベントを発行して外へ知らせる
		textfield_counter.on("change", function () {
			me.fireEvent("value_changed", me);
		});
		/// 増加ボタンのハンドラ設定
		button_increment.on("click", function () {
			me.setValue(counter + 1);
		});
		/// DOM オブジェクトは描画後でないと存在しない
		me.on("afterrender", function () {
			var button_decrement = me.getEl().down("input.decrement");
			/// 減少ボタン（DOM オブジェクト）のハンドラ設定
			button_decrement.on("click", function (ev, el, o) {
				me.setValue(counter - 1)
			})
		});
		me.setValue(0);
	},
})
